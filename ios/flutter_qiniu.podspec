#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
Pod::Spec.new do |s|
  s.name             = 'flutter_qiniu'
  s.version          = '0.0.1'
  s.summary          = 'A Qiniu SDK wrapper Flutter plugin.'
  s.description      = <<-DESC
A Qiniu SDK wrapper Flutter plugin.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.dependency 'Qiniu', '7.2.5'

  s.ios.deployment_target = '8.0'
end

