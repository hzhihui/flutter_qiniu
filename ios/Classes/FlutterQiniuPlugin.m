#import "FlutterQiniuPlugin.h"
#import "QiniuSDK.h"
#import "NSDictionary+ValidObject.h"

#import "QNUrlSafeBase64.h"
#import "UDJsonKit.h"

@interface FlutterQiniuPlugin ()
@property (nonatomic, strong) QNUploadManager *uploader;
@end

@implementation FlutterQiniuPlugin

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    FlutterMethodChannel* channel = [FlutterMethodChannel methodChannelWithName:@"hzh/flutter_qiniu" binaryMessenger:[registrar messenger]];
    FlutterQiniuPlugin* instance = [FlutterQiniuPlugin sharedInstance];
    [registrar addMethodCallDelegate:instance channel:channel];
}

#pragma mark - FlutterQiniuPlugin

+ (instancetype)sharedInstance {
    static FlutterQiniuPlugin *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    if ([@"getPlatformVersion" isEqualToString:call.method]) {
        result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
    } else if ([@"configure" isEqualToString:call.method]){
        [self configure:call.arguments result:result];
    } else if ([@"putFile" isEqualToString:call.method]){
        [self putFile:call.arguments result:result];
    } else if ([@"putData" isEqualToString:call.method]){
        [self putData:call.arguments result:result];
    } else if ([@"getUploadToken" isEqualToString:call.method]){
        [self getUploadToken:call.arguments result:result];
    } else {
        result(FlutterMethodNotImplemented);
    }
}

#pragma mark - Handler

/**
 *  arguments keys:
 *  ...."useHttps" | "zone" | "filePath" | "data" | "key" | "token"
 *  result keys:
 *  ...."code" | "message" | "response"
 */

- (void)configure:(id)arguments result:(FlutterResult)result
{
    if (_uploader != nil) {
        result(@"Configure Success.");
        return;
    }
    
    if (![arguments isKindOfClass:[NSDictionary class]]) {
        result([self errorWithMessage:@"Arguments Error."]);
        return;
    }
    
    BOOL useHttps = [[arguments validNumberForKey:@"useHttps" defaultValue:@(NO)] boolValue];
    NSString *zoneName = [arguments validStringForKey:@"zone"];
    
    QNFixedZone *zone = [self zoneWithName:zoneName];
    
    QNConfiguration *configuration = [QNConfiguration build:^(QNConfigurationBuilder *builder) {
        builder.useHttps = useHttps;
        if (zone) {
            builder.zone = zone;
        }
    }];
    
    _uploader = [QNUploadManager sharedInstanceWithConfiguration:configuration];
    
    result(@"Configure Success.");
}

- (void)getUploadToken:(id)arguments result:(FlutterResult)result
{
    if (![arguments isKindOfClass:[NSDictionary class]]) {
        result([self errorWithMessage:@"Arguments Error."]);
        return;
    }
    
    NSString *bucket = [arguments validStringForKey:@"bucket"];
    
    result([self createTokenWithBucket:bucket]);
}

- (void)putFile:(id)arguments result:(FlutterResult)result
{
    if (![arguments isKindOfClass:[NSDictionary class]]) {
        result([self errorWithMessage:@"Arguments Error."]);
        return;
    }
    
    NSString *filePath = [arguments validStringForKey:@"filePath"];
    NSString *key = [arguments validStringForKey:@"key"];
    NSString *token = [arguments validStringForKey:@"token"];
    
    if (!key.length || !key.length || !token.length) {
        result([self errorWithMessage:@"Arguments Error."]);
        return;
    }
    
    [_uploader putFile:filePath key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        
        if (info.error) {
            result([self errorWithCode:info.error.code message:info.error.localizedDescription]);
        } else {
            result([resp validStringForKey:@"key"]);
        }
    } option:nil];
}

- (void)putData:(id)arguments result:(FlutterResult)result
{
    if (![arguments isKindOfClass:[NSDictionary class]]) {
        result([self errorWithMessage:@"Arguments Error."]);
        return;
    }
    
    FlutterStandardTypedData *data = [arguments objectForKey:@"data"];
    NSString *key = [arguments validStringForKey:@"key"];
    NSString *token = [arguments validStringForKey:@"token"];
    
    if (![data isKindOfClass:[FlutterStandardTypedData class]] || !key.length || !token.length) {
        result([self errorWithMessage:@"Arguments Error."]);
        return;
    }
    
    [_uploader putData:data.data key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        
        if (info.error) {
            result([self errorWithCode:info.error.code message:info.error.localizedDescription]);
        } else {
            result([resp validStringForKey:@"key"]);
        }
    } option:nil];
}

#pragma mark - Private Action

- (QNFixedZone *)zoneWithName:(NSString *)name
{
    if (!name.length || ![[QNFixedZone class] respondsToSelector:NSSelectorFromString(name)]) {
        return nil;
    }
    
    SEL selector = NSSelectorFromString(name);
    IMP imp = [[QNFixedZone class] methodForSelector:selector];
    id (*func)(id, SEL) = (void *)imp;
    QNFixedZone *zone = func([QNFixedZone class], selector);
    
    return zone;
}

- (FlutterError *)errorWithMessage:(NSString *)message
{
    return [self errorWithCode:-100 message:message];
}

- (FlutterError *)errorWithCode:(NSInteger)code message:(NSString *)message
{
    return [FlutterError errorWithCode:[@(code) stringValue] message:message details:nil];
}




static NSString * const UDUploaderQiNiuAccessKey = @"hcHMmEX24Vu5gF5hTMXklFiTeML3XMWSNJZacPUC";
static NSString * const UDUploaderQiNiuSecretKey = @"eHBSQxH9GIp8kLw1OeHUSai1X46UOOHlSiuvozts";

static NSString * const UDUploaderTokenPolicyScope    = @"scope";
static NSString * const UDUploaderTokenPolicyDeadline = @"deadline";

- (NSString *)createTokenWithBucket:(NSString *)bucket
{
    NSNumber *deadline = @((uint64_t)[[NSDate date] timeIntervalSince1970] + 3600);
    
    NSMutableDictionary *policyDic = [NSMutableDictionary dictionary];
    
    [policyDic setObject:bucket forKey:UDUploaderTokenPolicyScope];
    [policyDic setObject:deadline forKey:UDUploaderTokenPolicyDeadline];
    

    NSString *policy = [policyDic ud_JSONString];
    NSString *encodedPolicy = [QNUrlSafeBase64 encodeString:policy];
    
    NSString *signString = [encodedPolicy hmacSHA1StringWithKey:UDUploaderQiNiuSecretKey]; //HexString
    NSString *encodedSign = [QNUrlSafeBase64 encodeData:[NSData dataWithHexString:signString]];
    
    /**
     *  uploadToken = AccessKey + ':' + encodedSign + ':' + encodedPolicy
     */
    
    NSMutableString *tokenString = [[NSMutableString alloc] init];
    
    [tokenString appendString:UDUploaderQiNiuAccessKey];
    [tokenString appendString:@":"];
    [tokenString appendString:encodedSign];
    [tokenString appendString:@":"];
    [tokenString appendString:encodedPolicy];
    
    return tokenString;
}

@end
