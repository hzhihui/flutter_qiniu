//
//  UDJsonKit.h
//  BSGridCollectionViewLayout
//
//  Created by CHEN on 2019/6/5.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (UDJsonKit)

- (NSString *)ud_JSONString;
- (NSData *)ud_JSONData;

@end

@interface NSArray (UDJsonKit)

- (NSString *)ud_JSONString;
- (NSData *)ud_JSONData;

@end

@interface NSString (UDJsonKit)

- (id)ud_objectFromJSONString;
- (NSString *)hmacSHA1StringWithKey:(NSString *)key;

@end

@interface NSData(UDJsonKit)
- (NSString *)hmacSHA1StringWithKey:(NSString *)key;

+ (NSData *)dataWithHexString:(NSString *)hexStr;

@end
