//
//  NSDictionary+ValidObject.m
//  flutter_qiniu
//
//  Created by CHEN on 2019/4/8.
//

#import "NSDictionary+ValidObject.h"

@implementation NSDictionary (ValidObject)

- (NSNumber *)validNumberForKey:(NSString *)key defaultValue:(NSNumber *)defaultValue
{
    if(!key)
    {
        return defaultValue;
    }
    
    id object = [self objectForKey:key];
    if(![object isKindOfClass:[NSNumber class]])
    {
        if([object isKindOfClass:[NSString class]])
        {
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            formatter.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *number = [formatter numberFromString:object];
            if(number)
            {
                return number;
            }
            else
            {
                return defaultValue;
            }
        }
        
        return defaultValue;
    }
    
    return object;
}

- (NSNumber *)validNumberForKey:(NSString *)key
{
    return [self validNumberForKey:key defaultValue:nil];
}

- (NSString *)validStringForKey:(NSString *)key defaultValue:(NSString *)defaultValue
{
    if(!key)
    {
        return defaultValue;
    }
    
    id object = [self objectForKey:key];
    if(![object isKindOfClass:[NSString class]])
    {
        if([object isKindOfClass:[NSNumber class]])
        {
            return [object stringValue];
        }
        
        return defaultValue;
    }
    
    return object;
}

- (NSString *)validStringForKey:(NSString *)key
{
    return [self validStringForKey:key defaultValue:nil];
}

@end
