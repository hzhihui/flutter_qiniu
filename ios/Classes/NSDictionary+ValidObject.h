//
//  NSDictionary+ValidObject.h
//  flutter_qiniu
//
//  Created by CHEN on 2019/4/8.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (ValidObject)
- (NSNumber *)validNumberForKey:(NSString *)key defaultValue:(NSNumber *)defaultValue;
- (NSNumber *)validNumberForKey:(NSString *)key;
- (NSString *)validStringForKey:(NSString *)key defaultValue:(NSString *)defaultValue;
- (NSString *)validStringForKey:(NSString *)key;
@end
