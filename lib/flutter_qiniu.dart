import 'dart:async';
import 'dart:typed_data';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:convert/convert.dart';
import 'package:flutter/services.dart';

class FlutterQiniu {
  static const String zone0 = "zone0";
  static const String zone1 = "zone1";
  static const String zone2 = "zone2";
  static const String zoneNa0 = "zoneNa0";
  static const String zoneAs0 = "zoneAs0";

  static const MethodChannel _channel = const MethodChannel('hzh/flutter_qiniu');
  /*
   * configureQiniu
   *    zone: "zone0"|"zone1"|"zone2"|"zoneNa0"|"zoneAs0"
   */
  static Future<String> configureQiniu(String zone, [bool useHttps = false]) async {
    Map<String, dynamic> arguments = {
      "zone": zone,
      "useHttps": useHttps,
    };
    return await _channel.invokeMethod('configure', arguments);
  }

  static Future<String> getUploadToken(String bucket) async {
    Map<String, String> arguments = {
      "bucket": bucket,
    };
    return await _channel.invokeMethod('getUploadToken', arguments);
  }

  /*
   * putFile
   */
  static Future<String> putFile(String filePath, String key, String token) async {
    Map<String, String> arguments = {
      "filePath": filePath,
      "key": key,
      "token": token,
    };
    return await _channel.invokeMethod('putFile', arguments);
  }

  /*
   * putData
   */
  static Future<String> putData(Uint8List data, String key, String token) async {
    Map<String, dynamic> arguments = {
      "data": data,
      "key": key,
      "token": token,
    };
    return await _channel.invokeMethod('putData', arguments);
  }
}

///
/// 七牛Token本地生成
/// 使用前先设置七牛AccessKey和SecretKey
/// 「官方不推荐在客户端处理，应该在服务端生成后返回给客户端」

class QiniuUploadToken {
  static String qiNiuAccessKey = "*";
  static String qiNiuSecretKey = "*";

  static String _policyScope    = "scope";
  static String _policyDeadline = "deadline";

  static String create(String bucket) {

    Map policyDic = _policyDic(bucket);

    String policy = jsonEncode(policyDic);
    String encodedPolicy = base64UrlEncode(utf8.encode(policy));

    var hmacSha256 = Hmac(sha1, utf8.encode(qiNiuSecretKey));
    var digest = hmacSha256.convert(utf8.encode(encodedPolicy));

    String signString = digest.toString();
    String encodedSign = base64UrlEncode(hex.decode(signString));

    return qiNiuAccessKey + ":" + encodedSign + ":" + encodedPolicy;
  }

  static Map _policyDic(String bucket) {
    String scope = bucket;
    int deadline = (DateTime.now().millisecondsSinceEpoch / 1000.0 + 3600).toInt();

    return {_policyScope: scope, _policyDeadline: deadline};
  }
}
