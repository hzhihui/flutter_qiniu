package hzh.flutter_qiniu;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import com.qiniu.android.common.FixedZone;
import com.qiniu.android.common.Zone;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;

import org.json.JSONObject;

/** FlutterQiniuPlugin */

public class FlutterQiniuPlugin implements MethodCallHandler {
    /** Plugin registration. */
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "hzh/flutter_qiniu");
        channel.setMethodCallHandler(FlutterQiniuPlugin.getInstance());
    }

    private static class SingletonHolder {
        private static final FlutterQiniuPlugin INSTANCE = new FlutterQiniuPlugin();
    }
    private FlutterQiniuPlugin(){}
    public static FlutterQiniuPlugin getInstance() {
        return SingletonHolder.INSTANCE;
    }



    /**
     * uploader
     */
    public UploadManager uploader;

    /**
     *  arguments keys:
     *  ...."useHttps" | "zone" | "filePath" | "data" | "key" | "token"
     *  result keys:
     *  ...."code" | "message" | "response"
     */

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else if (call.method.equals("configure")) {
            configure(call, result);
        } else if (call.method.equals("putFile")) {
            putFile(call, result);
        } else if (call.method.equals("putData")) {
            putData(call, result);
        } else {
            result.notImplemented();
        }
    }

    private void configure(MethodCall call, final Result result) {
        Boolean useHttps = call.argument("useHttps");
        String zoneName = call.argument("zone");

        Zone zone = getZone(zoneName);

        Configuration configuration = new Configuration.Builder()
                .useHttps(useHttps.booleanValue())
                .zone(zone)
                .build();

        uploader = new UploadManager(configuration);

        result.success("Configure Success.");
    }

    private void putFile(MethodCall call, final Result result) {
        String filePath = call.argument("filePath");
        String key = call.argument("key");
        String token = call.argument("token");

        uploader.put(filePath, key, token,
                new UpCompletionHandler() {
                    @Override
                    public void complete(String key, ResponseInfo info, JSONObject res) {
                        if (info.isOK()) {
                            result.success(res.opt("key"));
                        } else {
                            result.error(String.valueOf(info.statusCode),info.error,null);
                        }
                    }
                }, null);

    }

    private void putData(MethodCall call, final Result result) {
        byte[] data = call.argument("data");
        String key = call.argument("key");
        String token = call.argument("token");

        uploader.put(data, key, token,
                new UpCompletionHandler() {
                    @Override
                    public void complete(String key, ResponseInfo info, JSONObject res) {
                        if (info.isOK()) {
                            result.success(res.opt("key"));
                        } else {
                            result.error(String.valueOf(info.statusCode),info.error,null);
                        }
                    }
                }, null);
    }

    /**
     * Private Action
     */

    private Zone getZone(String zoneName) {
        Zone zone = null;

        switch (zoneName) {
            case "zone0":
                zone = FixedZone.zone0;
                break;
            case "zone1":
                zone = FixedZone.zone1;
                break;
            case "zone2":
                zone = FixedZone.zone2;
                break;
            case "zoneNa0":
                zone = FixedZone.zoneNa0;
                break;
            case "zoneAs0":
                zone = FixedZone.zoneAs0;
                break;
        }
        return zone;
    }
}
